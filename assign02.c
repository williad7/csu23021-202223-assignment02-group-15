#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>

//LED Libraries
#include "pico/stdlib.h"
#include "hardware/pio.h"
#include "hardware/clocks.h"
#include "assign02.pio.h"   // or is this .pio.h

//GPIO button library
#include "hardware/gpio.h"

//Define LED variables
#define IS_RGBW true        // Will use RGBW format
#define NUM_PIXELS 1        // There is 1 WS2812 device in the chain
#define WS2812_PIN 28       // The GPIO pin that the WS2812 connected to

//Game variables
int lives = 3; //Number of lives player has
int level = 1; //For choice of user level
int game_started = 1; //Will start game if true
int level_started = 1;
int SEQUENCE_SIZE = 20;
char sequence[20];
int index_of_sequence = 0;
int check_sequence = 0;
int index_of_correct_morse;
int character;
int check_answer = 1;
int correct_answers = 0;

//Morse Mapping
char char_array[] = {
    // Digits 0 - 9
    '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 
    // Letters A - Z
    'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'};
char morse_table[36][5] = {
    // Digits 0 - 9
    "-----", ".----", "..---", "...--", "....-", ".....", "-....", "--...", "---..", "----.", 
    // Letters A - Z
    ".-", "-...", "-.-.", "-..", ".", "..-.", "--.", "....", "..", ".---", "-.-", ".-..", "--", "-.", "---", ".--.",
    "--.-", ".-.", "...", "-", "..-", "...-", ".--", "-..-", "-.--", "--..",
}; 

// Functions
// Must declare the main assembly entry point before use.
void main_asm();

// LED Functions
static inline void put_pixel(uint32_t pixel_grb) {
    pio_sm_put_blocking(pio0, 0, pixel_grb << 8u);
}

static inline uint32_t urgb_u32(uint8_t r, uint8_t g, uint8_t b) {
    return ((uint32_t)(r) << 8) | ((uint32_t)(g) << 16) | (uint32_t)(b);
}

//Controls LED colour based on game
static inline void update_led() {
    if (game_started == 0) {
        // Set LED to BLUE once the game opens but hasnt started
        put_pixel(urgb_u32(0x00, 0x00, 0xFF));
    }
    else {
        if (lives >= 3)
        // GREEN
        put_pixel(urgb_u32(0x00, 0xFF, 0x00));

        if (lives == 2)
        // YELLOW
        put_pixel(urgb_u32(0xFF, 0xFF, 0x00));

        if (lives == 1)
        // ORANGE
        put_pixel(urgb_u32(0xFF, 0xA0, 0x11));

        if (lives == 0)
        // RED
        put_pixel(urgb_u32(0xFF, 0x00, 0x00));
    }
}

void checkInput(int value) {
    if(game_started == 1) {
            if(level_started == 1) {
                if(index_of_sequence < SEQUENCE_SIZE) {
                    if(value == 0) {
                        sequence[index_of_sequence] = '.';
                        index_of_sequence++;
                    }
                    else if(value == 1) {
                        sequence[index_of_sequence] = '-';
                        index_of_sequence++;
                    }
                    else if(value == 2) {
                        sequence[index_of_sequence] = ' ';
                        index_of_sequence++;
                    }
                    else {
                        check_sequence = 1;
                        level_started = 0;
                    }
                }
                else {
                    sequence[SEQUENCE_SIZE-1] = "?";
                    check_sequence = 1;
                    level_started = 0;
                }
            }
            else {
                if(check_sequence == 1) {
                    int index_of_morse = -1;
                    for(int i = 0; i < 36; i++) {
                        int is_matching = 1;
                        for(int j = 0; j<5; j++) {
                            if(sequence[j] != morse_table[i][j]) {
                                is_matching = 0;
                            }
                        }
                        if(is_matching == 1) {
                            index_of_morse = i;
                        }
                    }
                    printf("Your sequence is: ");
                    for(int i = 0; i<5; i++) {
                        printf("%c", sequence[i]);
                    }
                    printf("\nThis corresponds to: ");
                    if(index_of_morse < 0) {
                        printf("?\n");
                    }
                    else {
                        printf("%c\n", char_array[index_of_morse]);
                    }
                    if(check_answer == 1) {
                        if(index_of_morse == character) {
                           printf("Well Done!");
                           if(lives < 3) {
                                lives++;
                           } 
                           update_led();
                           check_answer = 0;
                           printf("You have %d lives remaining.", lives);
                           if(correct_answers == 5) {
                                printf("You have advanced to the next level");
                                level = 2;
                                correct_answers = 0;
                           }
                           else {
                                correct_answers++;
                           }
                        }
                        else {
                            printf("That was incorrect.");
                            if(lives > 1) {
                                lives--;
                                update_led();
                                check_answer = 0;
                                printf("You have %d lives remaining.", lives);
                                correct_answers = 0;
                            }
                            else {
                                printf("You have lost the game.");
                                game_started = 0;
                                correct_answers = 0;
                            }
                        }
                    }
                    for(int i = 0; i<SEQUENCE_SIZE; i++) {
                        sequence[i] = 0;
                    }
                    index_of_sequence = 0;
                    check_sequence = 0;

                }
                else {
                    int is_matching = 1;
                    for(int i = 0; i<5; i++) {
                        if(sequence[i] != morse_table[1][i]) {
                            is_matching = 0;
                        }
                    }
                    if(is_matching == 1) {
                        level = 1;
                        game_started = 1;
                    }
                    else {
                        is_matching = 1;
                        for(int i = 0; i<5; i++) {
                            if(sequence[i] != morse_table[2][i]) {
                                is_matching = 0;
                            }
                        }
                        if(is_matching == 1) {
                            level = 2;
                            game_started = 1;
                        }
                        else {
                            level = 0;
                            game_started = 0;
                        }
                    }
                    for(int i = 0; i<SEQUENCE_SIZE; i++) {
                        sequence[i] = 0;
                    }
                    index_of_sequence = 0;
                    check_sequence = 0;
                }
            }

    }
    else {
        printf("Please select a level using the corresponding morse code sequence\n");
        printf("Level 1 (.----): Morse Code Is Shown\n");
        printf("Level 2 (..---): Morse Code Is Not Shown\n");
        level_started = 1;
        game_started = 1;
        update_led();
    }
    if(level == 1 && check_sequence == 0) {
        character = (rand() % (36));
        printf("Your character is %c\n", char_array[character]);
        printf("The morse code sequence is: ");
        for(int i = 0; i < 5; i++) {
            printf("%c", morse_table[character][i]);
        }
        printf("\n");
        level_started = 1;
        check_answer = 1;
        update_led();
    }
    else if(level == 2 && check_sequence == 0) {
        character = (rand() % (36));
        printf("Your character is %c\n", char_array[character]);
        printf("\n");
        level_started = 1;
        check_answer = 1;
        update_led();
    }
}

// GPIO Functions
// Initialise a GPIO pin – see SDK for detail on gpio_init()
void asm_gpio_init(uint pin) {
    gpio_init(pin);
}

// Set direction of a GPIO pin – see SDK for detail on gpio_set_dir()
void asm_gpio_set_dir(uint pin, bool out) {
    gpio_set_dir(pin, out);
}

// Get the value of a GPIO pin – see SDK for detail on gpio_get()
bool asm_gpio_get(uint pin) {
    return gpio_get(pin);
}

// Set the value of a GPIO pin – see SDK for detail on gpio_put()
void asm_gpio_put(uint pin, bool value) {
    gpio_put(pin, value);
}

// Enable falling-edge interrupt – see SDK for detail on gpio_set_irq_enabled()
void asm_gpio_set_irq(uint pin) {
    gpio_set_irq_enabled(pin, GPIO_IRQ_EDGE_RISE | GPIO_IRQ_EDGE_FALL, true);
}

/*
 * Main entry point for the code - simply calls the main assembly function.
 */
int main() {
    stdio_init_all();
    update_led();
    printf("Welcome! We are group 15.\n");
    printf("RULES OF THE GAME\n");
    printf("To play the game, you must first choose a level\n");
    printf("Level 1 will show you a character and the morse code sequence\n");
    printf("Level 2 will just give a character\n");
    printf("To mimic the morse code, you just press the GP21 button, around half a second for a dot and over for a dash\n");
    printf("Good Luck! :D\n");
    main_asm();
    return(0);
}


